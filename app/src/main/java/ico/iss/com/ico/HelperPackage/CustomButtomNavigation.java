package ico.iss.com.ico.HelperPackage;

/*All used classes are imported here*/

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;

import java.lang.reflect.Field;

import ico.iss.com.ico.R;

/**
 * written by Md. Mujahid Khan on 13/09/2018
 */
public class CustomButtomNavigation {
    @SuppressLint("RestrictedApi")

    /*this method disable shifting Animation of bottom navigation bar*/
    public static void removeShiftMode(BottomNavigationView view, Activity activity) {

        /*get first bottom navigation menu view from bottom navigation  */
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);


        try {
            /*get field of bottom view from menu view */
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            /*set accessible true to Field*/
            shiftingMode.setAccessible(true);
            /*set boolean to field with menu b*/
            shiftingMode.setBoolean(menuView, false);
            /*then set accessible false to field */
            shiftingMode.setAccessible(false);
            /*for every menu view in the bottom navigation set shifting mode false and set checked the item data is checked*/
            for (int i = 0; i < menuView.getChildCount(); i++) {

                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);

                if(i == 2){

                    item.setPadding(0, 25, 0, 0);

                    item.setItemBackground(R.drawable.bottom_navigation_btn_bg);
                   // item.setBackgroundResource(R.drawable.bottom_navigation_btn_bg);
                }


                item.setPadding(0, 15, 0, 0);


            /*    AppCompatImageView icon = (AppCompatImageView) item.getChildAt(0);

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) icon.getLayoutParams();

                params.gravity = Gravity.CENTER;*/


                item.setShiftingMode(false);

                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());


            }
        } catch (NoSuchFieldException e) {
            Log.e("ERROR NO SUCH FIELD", "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            Log.e("ERROR ILLEGAL ALG", "Unable to change value of shift mode");
        }
    }
}
