package ico.iss.com.ico.AdapterPackage;

/*all used classes are imported here*/
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class DashboardViewPagerAdapter extends FragmentPagerAdapter {

    /*all global filed instances are here*/

    List<Fragment> fragmentList = new ArrayList<>();
    List<String> titles = new ArrayList<>();

    /**
     *constructor for getting FragmentManager and setting that to FragmentPagerAdapter class
     * @param fm
     */
    public DashboardViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    /**
     * return the current fragment from the fragmentList
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    /**
     * add fragment and title to the list
     * @param fragment
     * @param title
     */
    public void addFragment(Fragment fragment, String title) {
        fragmentList.add(fragment);
        titles.add(title);
    }


    /**
     *return the title for the current fragment
     * @param position
     * @return
     */
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        return titles.get(position);
    }


    /**
     *return the fragment list size
     * @return
     */
    @Override
    public int getCount() {

        return fragmentList.size();
    }


}
