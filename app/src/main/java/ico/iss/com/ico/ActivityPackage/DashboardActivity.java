package ico.iss.com.ico.ActivityPackage;

/*ALl used classes are imported here*/

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import ico.iss.com.ico.FragmentPackage.AddWalletFragment;
import ico.iss.com.ico.FragmentPackage.BuyTokenFragment;
import ico.iss.com.ico.FragmentPackage.DepositFragment;
import ico.iss.com.ico.FragmentPackage.HomeFragment;
import ico.iss.com.ico.FragmentPackage.ReferalFragment;
import ico.iss.com.ico.FragmentPackage.SettingFragment;
import ico.iss.com.ico.FragmentPackage.UserFragment;
import ico.iss.com.ico.FragmentPackage.WalletFragment;
import ico.iss.com.ico.FragmentPackage.WithdrawFragment;
import ico.iss.com.ico.HelperPackage.Util;
import ico.iss.com.ico.R;

public class DashboardActivity extends AppCompatActivity {

    /*All global field instances are here*/
    int count = 0;
    TextView dash_title;
    Fragment fragment;
    FrameLayout frameLayout;
    FrameLayout bottomNavigation;
    LinearLayout mnHome, mnWallet, mnReffer, mnUser;
    ImageButton buyCoin, ic_Home, ic_wallet, ic_reffer, ic_user;
    ImageView btnSetting, btnSearch;
    String titleKeeper;
    Fragment lastFragment;
    ImageView btnBack;


    /**
     * Activity life cycle function, this function is called when app runs
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dashboard);


        /*all views type casting here*/
        initView();


        /*bottom menu selector functionality */
        BottomMenuSelectorBuilder();

        /*select home menu as default*/
        DefaultMenuSelector();

        /*click event fro buy coin button this button is the middle big button*/
        buyCoinIconClick();

        /*set Home fragment as default fragment*/
        FragmentTransaction(new HomeFragment());

        /*click event for setting button*/
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*set setting fragment on top of backstack*/
                FragmentTransaction(new SettingFragment());
                /*set bottom menu bar margin_bottom to zero */
                setFrameBottomMarginZero();

            }
        });

        /*click event for the back button*/
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*call the onBackPressed() function*/
                onBackPressed();
            }
        });


    }

    /*this function sets bottom menus margin bottom to zero*/
    private void setFrameBottomMarginZero() {
        /*get he framelayout paras and convert to its' parent LayoutParams*/
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
        if (layoutParams.bottomMargin > 50) {
            /*set layout params to zero */
            layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 0));
            /*set the layout params to the framelayout*/
            frameLayout.setLayoutParams(layoutParams);
        }
    }

    /*all views are type casted here*/
    private void initView() {
        mnHome = findViewById(R.id.mn_home);
        mnReffer = findViewById(R.id.mn_reffer);
        mnUser = findViewById(R.id.mn_user);
        buyCoin = findViewById(R.id.buy_coin);
        mnWallet = findViewById(R.id.mn_wallet);
        dash_title = findViewById(R.id.dash_title);
        frameLayout = findViewById(R.id.dash_container);
        bottomNavigation = findViewById(R.id.bottom_navigation);
        btnSetting = findViewById(R.id.btn_setting);
        btnSearch = findViewById(R.id.btn_search);
        btnBack = findViewById(R.id.btn_back);
        ic_Home = findViewById(R.id.ic_home);
        ic_wallet = findViewById(R.id.ic_wallet);
        ic_reffer = findViewById(R.id.ic_refer);
        ic_user = findViewById(R.id.ic_user);

        /*set tag to buyCoin button as true*/
        buyCoin.setTag(true);
        //bottomNavigationView = findViewById(R.id.bottom_bar);
    }

    /**
     * replace current fragment with a new fragment
     * every fragment have it's own new features will check each fragment and will customize some views
     *
     * @param fragment
     */
    public void FragmentTransaction(Fragment fragment) {

        /*set count variable  0*/
        count = 0;

        /*assign new fragment to the global fragment*/
        this.fragment = fragment;


        /*if new fragment is AddWalletFragment then hide bottom navigation menus, set title to Add New Wallet, set back button visible,
         * set title text margin to 0
         * set bottom menus bottom margin to 0
         * */
        if (fragment instanceof AddWalletFragment) {

            bottomNavigation.setVisibility(View.GONE);
            dash_title.setText("Add New Wallet");
            btnBack.setVisibility(View.VISIBLE);

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) dash_title.getLayoutParams();
            layoutParams.setMargins((int) Util.DpToPx(DashboardActivity.this, 0), 0, 0, 0);

            dash_title.setLayoutParams(layoutParams);

            /*change container bottom margin to 0 if have 50*/
            RelativeLayout.LayoutParams BottomParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
            if (BottomParams.bottomMargin > 50) {
                BottomParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 0));
                frameLayout.setLayoutParams(BottomParams);
            }

            /*if new fragment is SettingFragment then hide bottom navigation menus, set title to Setting, set back button visible,
             * hide search and setting button
             * set title text margin to 0
             *
             * */

        } else if (fragment instanceof SettingFragment) {
            btnBack.setVisibility(View.VISIBLE);
            bottomNavigation.setVisibility(View.GONE);
            btnSearch.setVisibility(View.GONE);
            btnSetting.setVisibility(View.GONE);
            dash_title.setText("Setting");
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) dash_title.getLayoutParams();
            layoutParams.setMargins((int) Util.DpToPx(DashboardActivity.this, 0), 0, 0, 0);
            dash_title.setLayoutParams(layoutParams);

            /*if new fragment is DepositFragment then hide bottom navigation menus, set title to Deposit, set back button visible,
             * set bottom menus bottom margin to 0
             * set title text margin to 0
             *
             * */

        } else if (fragment instanceof DepositFragment) {
            btnBack.setVisibility(View.VISIBLE);
            bottomNavigation.setVisibility(View.GONE);
            dash_title.setText("Deposit");

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) dash_title.getLayoutParams();
            layoutParams.setMargins((int) Util.DpToPx(DashboardActivity.this, 0), 0, 0, 0);
            dash_title.setLayoutParams(layoutParams);

            /*change container bottom margin to 0 if have 50*/
            RelativeLayout.LayoutParams BottomParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
            if (BottomParams.bottomMargin > 50) {
                BottomParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 0));
                frameLayout.setLayoutParams(BottomParams);
            }

            /*if new fragment is WithdrawFragment then hide bottom navigation menus, set title to Withdraw, set back button visible,
             * set bottom menus bottom margin to 0
             * set title text margin to 0
             *
             * */
        } else if (fragment instanceof WithdrawFragment) {
            btnBack.setVisibility(View.VISIBLE);
            bottomNavigation.setVisibility(View.GONE);
            dash_title.setText("Withdraw");

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) dash_title.getLayoutParams();
            layoutParams.setMargins((int) Util.DpToPx(DashboardActivity.this, 0), 0, 0, 0);
            dash_title.setLayoutParams(layoutParams);

            /*change container bottom margin to 0 if have 50*/
            RelativeLayout.LayoutParams BottomParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
            if (BottomParams.bottomMargin > 50) {
                BottomParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 0));
                frameLayout.setLayoutParams(BottomParams);
            }


            /*if all of above don't matches, then do the bellow tasks */
        } else

        {

            /*set title text margin to 16 dp*/
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) dash_title.getLayoutParams();
            layoutParams.setMargins((int) Util.DpToPx(DashboardActivity.this, 16), 0, 0, 0);
            dash_title.setLayoutParams(layoutParams);

            /*set back button invisible*/
            btnBack.setVisibility(View.GONE);
            /*set search, setting and bottom menus visible*/
            btnSearch.setVisibility(View.VISIBLE);
            btnSetting.setVisibility(View.VISIBLE);
            bottomNavigation.setVisibility(View.VISIBLE);
        }

        /*if current fragment is home fragment set title to Home, set bottom menus margin to 55 dp */
        if (fragment instanceof HomeFragment) {

            dash_title.setText("Home");

            /*change container bottom margin to 0 if have 50*/
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
            layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 55));
            frameLayout.setLayoutParams(layoutParams);

            /*if current fragment is WalletFragment  set title to Wallet */
        } else if (fragment instanceof WalletFragment) {

            dash_title.setText("Wallet");

        }

        /*if current fragment is UserFragment  set title to Profile */
        if (fragment instanceof UserFragment) {

            dash_title.setText("Profile");

        }


        if (fragment instanceof BuyTokenFragment) {

        }


        /*build FragmentTransaction object by starting fragment manager transaction*/
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();


        /*if current fragment is BuyTokenFragment then set custom animation and add fragment instead of replacing fragment */
        if (fragment instanceof BuyTokenFragment) {

            fragmentTransaction.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
            fragmentTransaction.add(R.id.dash_container, fragment);

            /*else replace fragments instead of adding over another fragment*/
        } else {

            fragmentTransaction.replace(R.id.dash_container, fragment);
        }

        /*add fragment to back stack*/
        fragmentTransaction.addToBackStack(null);
        /*commit fragment transaction */
        fragmentTransaction.commit();

    }


    /*this is the state when the buyCoin button is in cancel state*/
    private void BuyCoinCancelState() {
        /*set boolean false to buycoin button as tag*/
        buyCoin.setTag(false);
        /*clear all the animation of buy coin button*/
        buyCoin.clearAnimation();
        /*set cancel icon to buyCoin button*/
        buyCoin.setImageResource(R.drawable.cancel);
        /*rotate buyCoin button 90 degree right with 300 milliseconds */
        buyCoin.animate().rotation(90).setDuration(300).start();
        /*replace current fragment with  BuyToken fragment*/
        FragmentTransaction(new BuyTokenFragment());
        /*set cancel icon to buyCoin button*/
        buyCoin.setImageResource(R.drawable.cancel);

    }

    /*this is the state when the buyCoin button is in buy state*/
    private void BuyCoinBuyState() {
        /*set boolean true to buycoin button as tag*/
        buyCoin.setTag(true);
        /*clear all the animation of buy coin button*/
        buyCoin.clearAnimation();
        /*set buy coin icon to buyCoin button*/
        buyCoin.setImageResource(R.drawable.ic_buy_coin);
        /*rotate buyCoin button 90 degree right with 300 milliseconds */
        buyCoin.animate().rotation(-90).setDuration(300).start();


        /*if fragment is BuyToken Fragment then remote the fragment from back stack*/
        if (fragment instanceof BuyTokenFragment) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(0, R.anim.slide_down);
            fragmentTransaction.remove(fragment).commit();
        }


        /*set buy coin icon to buyCoin button*/
        buyCoin.setImageResource(R.drawable.ic_buy_coin);
    }


    /*this is the click event for buyCoin Button*/
    private void buyCoinIconClick() {

        /*click listener for buy coin button*/
        buyCoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*if buyCoin tag is true then set title to the titleKeeper variable
                 * set title to BuyToken
                 * call BuyCoinCancelState function
                 * else set titleKeeper variable value to title
                 * call BuyCoinBuyState function
                 * */
                if ((Boolean) buyCoin.getTag()) {

                    titleKeeper = dash_title.getText().toString();

                    dash_title.setText("BuyToken");

                    BuyCoinCancelState();

                } else {

                    if (titleKeeper != null) {
                        dash_title.setText(titleKeeper);
                    }

                    BuyCoinBuyState();


                }


            }
        });

    }


    /**
     * this function sets functionality to bottom menu
     */
    private void BottomMenuSelectorBuilder() {

        /*click listener to home menu*/
        mnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*change container bottom margin to 0 if have 50*/
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
                if (layoutParams.bottomMargin > 50) {
                    layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 0));
                    frameLayout.setLayoutParams(layoutParams);
                }

                /*fragment transition*/

                FragmentTransaction(new HomeFragment());

                /*chane title*/

                dash_title.setText("Home");


                /*check buy coin state*/
                if (!(Boolean) buyCoin.getTag()) {
                    buyCoin.setTag(true);
                    BuyCoinBuyState();
                }

                /*onw*/
                ic_Home.setImageResource(R.drawable.ic_home_hover);
                /*other*/
                ic_user.setImageResource(R.drawable.ic_user);
                ic_reffer.setImageResource(R.drawable.ic_refferal);
                ic_wallet.setImageResource(R.drawable.ic_wallet);
            }
        });

        /*click listener for refer menu*/
        mnReffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*onw*/
                ic_reffer.setImageResource(R.drawable.ic_refferal_hover);

                /*check buy coin state*/
                if (!(Boolean) buyCoin.getTag()) {
                    buyCoin.setTag(true);
                    BuyCoinBuyState();
                }

                /*fragment transition*/
                FragmentTransaction(new ReferalFragment());

                /*chane title*/
                dash_title.setText("Referral");

                /*change container bottom margin to 0 if have 50*/
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
                if (layoutParams.bottomMargin > 50) {
                    layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 0));
                    frameLayout.setLayoutParams(layoutParams);
                }


                /*other*/
                ic_user.setImageResource(R.drawable.ic_user);
                ic_Home.setImageResource(R.drawable.ic_home);
                ic_wallet.setImageResource(R.drawable.ic_wallet);
            }
        });

        /*click listener for user menu*/
        mnUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*onw*/
                ic_user.setImageResource(R.drawable.ic_user_hover);

                /*check buy coin state*/
                if (!(Boolean) buyCoin.getTag()) {
                    buyCoin.setTag(true);
                    BuyCoinBuyState();
                }

                /*fragment transaction*/
                FragmentTransaction(new UserFragment());

                /*change container bottom margin to 0 if have 50*/
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
                if (layoutParams.bottomMargin > 50) {
                    layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 0));
                    frameLayout.setLayoutParams(layoutParams);
                }

                /*other*/
                ic_reffer.setImageResource(R.drawable.ic_refferal);
                ic_Home.setImageResource(R.drawable.ic_home);
                ic_wallet.setImageResource(R.drawable.ic_wallet);
            }
        });



        /*click listener for wallet menu*/
        mnWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*fragment transition*/

                FragmentTransaction(new WalletFragment());

                /*check buy coin state*/
                if (!(Boolean) buyCoin.getTag()) {
                    buyCoin.setTag(true);
                    BuyCoinBuyState();
                }

                /*set 55 dp margin to container*/

                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
                layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 55));
                frameLayout.setLayoutParams(layoutParams);

                /*chane title*/

                dash_title.setText("Wallet");

                /*onw*/
                ic_wallet.setImageResource(R.drawable.ic_wallet_hover);

                /*other*/
                ic_reffer.setImageResource(R.drawable.ic_refferal);
                ic_Home.setImageResource(R.drawable.ic_home);
                ic_user.setImageResource(R.drawable.ic_user);
            }
        });
    }

    /*default menu selected function, */
    private void DefaultMenuSelector() {

        /*onw*/
        ic_Home.setImageResource(R.drawable.ic_home_hover);

        /*other*/
        ic_user.setImageResource(R.drawable.ic_user);
        ic_reffer.setImageResource(R.drawable.ic_refferal);
        ic_wallet.setImageResource(R.drawable.ic_wallet);

    }


    /**
     * this function called when the user presses back button
     * we will check the current fragment and will replace to it's previous fragment
     * and if user is on the default fragment that is home fragment then we will check the click of the user
     * after clicking 2 times the app will be exit
     */
    @Override
    public void onBackPressed() {

        /*if fragment is HomeFragment then wait for 2 click by a counter variable when user click first time we are showing a toast message
         * if user click second time then the app will be exited
         * */

        if (fragment instanceof HomeFragment) {

            count++;

            if (count == 1) {
                Toast.makeText(this, "Press Again To Exit!", Toast.LENGTH_SHORT).show();
            } else if (count == 2) {

                finishAffinity();
                moveTaskToBack(true);
                System.exit(1);
                android.os.Process.killProcess(android.os.Process.myPid());
            }

            /*if fragment is AddWalletFragment then replace fragment with Wallet Fragment and set container bottom margin to 55 dp */

        } else if (fragment instanceof AddWalletFragment) {

            FragmentTransaction(new WalletFragment());


            /*change container bottom margin 55dp*/
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();

            layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 55));
            frameLayout.setLayoutParams(layoutParams);


            /*if fragment is WalletFragment then */
        } else if (fragment instanceof WalletFragment) {

            FragmentTransaction(new HomeFragment());


            /*change container bottom margin to 0 if have 50*/
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();

            layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 55));
            frameLayout.setLayoutParams(layoutParams);


            /*chane title*/

            dash_title.setText("Home");

            /*onw*/
            ic_Home.setImageResource(R.drawable.ic_home_hover);
            /*other*/
            ic_user.setImageResource(R.drawable.ic_user);
            ic_reffer.setImageResource(R.drawable.ic_refferal);
            ic_wallet.setImageResource(R.drawable.ic_wallet);

            /*if fragment is ReferalFragment then replace fragment with Wallet Fragment and set 55 dp bottom margin to container*/
        } else if (fragment instanceof ReferalFragment) {

            FragmentTransaction(new WalletFragment());

            /*change container bottom margin to 0 if have 50*/
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
            layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 55));
            frameLayout.setLayoutParams(layoutParams);


            /*chane title*/

            dash_title.setText("Wallet");

            /*onw*/
            ic_wallet.setImageResource(R.drawable.ic_wallet_hover);
            /*other*/
            ic_user.setImageResource(R.drawable.ic_user);
            ic_reffer.setImageResource(R.drawable.ic_refferal);
            ic_Home.setImageResource(R.drawable.ic_home);

            /*if fragment is userFragment then replace fragment with ReferaFragment and set margin to 0 dp*/
        } else if (fragment instanceof UserFragment) {

            FragmentTransaction(new ReferalFragment());

            /*change container bottom margin to 0 if have 50*/
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
            if (layoutParams.bottomMargin > 50) {
                layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 0));
                frameLayout.setLayoutParams(layoutParams);
            }

            /*chane title*/

            dash_title.setText("Referral");

            /*onw*/
            ic_reffer.setImageResource(R.drawable.ic_refferal_hover);
            /*other*/
            ic_user.setImageResource(R.drawable.ic_user);
            ic_wallet.setImageResource(R.drawable.ic_wallet);
            ic_Home.setImageResource(R.drawable.ic_home);

            /*if fragment is DepostiFragment or WithdrawFragment then set bottom margin to 55 dp and replace fragment with Wallet fragment */
        } else if (fragment instanceof DepositFragment || fragment instanceof WithdrawFragment) {

            /*change container bottom margin to 0 if have 50*/
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
            layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 55));
            frameLayout.setLayoutParams(layoutParams);


            FragmentTransaction(new WalletFragment());


            /*if fragment is settingFragment then set container margin to 0 and replace fragment with Home Fragment*/
        } else if (fragment instanceof SettingFragment) {

            /*change container bottom margin to 0 if have 50*/
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();
            if (layoutParams.bottomMargin > 50) {
                layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 0));
                frameLayout.setLayoutParams(layoutParams);
            }



            /*chane title*/

            dash_title.setText("Home");

            /*onw*/
            ic_Home.setImageResource(R.drawable.ic_home_hover);
            /*other*/
            ic_user.setImageResource(R.drawable.ic_user);
            ic_reffer.setImageResource(R.drawable.ic_refferal);
            ic_wallet.setImageResource(R.drawable.ic_wallet);

            FragmentTransaction(new HomeFragment());

        }


        /*if buyCoin fragment is on Top then set tag false and replace fragment with Homefragment and set container margin to 55*/
        if (!(Boolean) buyCoin.getTag()) {
            buyCoin.setTag(true);
            BuyCoinBuyState();
            FragmentTransaction(new HomeFragment());

            /*change container bottom margin to 0 if have 50*/
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frameLayout.getLayoutParams();

            layoutParams.setMargins(0, 0, 0, (int) Util.DpToPx(DashboardActivity.this, 55));
            frameLayout.setLayoutParams(layoutParams);


            /*chane title*/

            dash_title.setText("Home");

            /*onw*/
            ic_Home.setImageResource(R.drawable.ic_home_hover);
            /*other*/
            ic_user.setImageResource(R.drawable.ic_user);
            ic_reffer.setImageResource(R.drawable.ic_refferal);
            ic_wallet.setImageResource(R.drawable.ic_wallet);
        }

    }
}
