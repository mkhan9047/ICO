package ico.iss.com.ico.FragmentPackage;

/*all used classes are imported here*/

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import ico.iss.com.ico.HelperPackage.Util;
import ico.iss.com.ico.R;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class AddWalletFragment extends Fragment {

    /*all global field instances*/

    LinearLayout uploadBtn;
    TextView fileName;


    public AddWalletFragment() {
        // Required empty public constructor
    }


    /**
     * fragment lifecycle method for inflating the layout for this fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_wallet, container, false);
    }


    /**
     * fragment lifecycle method called after fragment is attached to a activity all view type casting and data binding will do here
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*all views are type casting */
        View view = getView();

        if (view != null) {
            uploadBtn = view.findViewById(R.id.upload_file);
            fileName = view.findViewById(R.id.file_name);
        }


        /*upload click listener, open the file open dialog  */
        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenFileDilog();
            }
        });
    }

    /*The result of the file selector intent will trigger here after the file is selected */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*if the file selection is successful then get the uri of the file*/
        if (requestCode == 123 && resultCode == RESULT_OK) {
            Uri selectedItem = data.getData();
            if (selectedItem != null) {
                /*get the file name from the file uri and show in the fileName text view*/
                /*we made a function named getFileNameFromUri see in the Helper class*/
                fileName.setText(Util.getFileNameFromUri(selectedItem, getContext()));
            }
        }
    }


    /*THis method will open the file selector section by intent we have set the type to all, you can set any type as filter*/
    private void OpenFileDilog() {
        /*make a intent object*/
        Intent intent = new Intent()
                /*set type*/
                .setType("*/*")
                /*set action*/
                .setAction(Intent.ACTION_GET_CONTENT);

        /*start the intent and wait for result */
        startActivityForResult(Intent.createChooser(intent, "Select a file"), 123);
    }

}
