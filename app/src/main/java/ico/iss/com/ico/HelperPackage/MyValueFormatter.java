package ico.iss.com.ico.HelperPackage;

/*all used classes are imported here*/
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

public class MyValueFormatter implements IValueFormatter {

    /*all global field instances are here*/
    private DecimalFormat decimalFormat;

    /*set value formatting format*/
    public MyValueFormatter(){
        decimalFormat = new DecimalFormat("###,###,##0");
    }

    /*return the formatted value */
    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        return decimalFormat.format(value) + "%";
    }
}
