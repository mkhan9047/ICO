package ico.iss.com.ico.FragmentPackage;

/*all used classes are imported here*/

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import ico.iss.com.ico.HelperPackage.MyValueFormatter;
import ico.iss.com.ico.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class TokenFragment extends Fragment {


    /*all global field instances are here*/
    PieChart pieChart;

    public TokenFragment() {
        // Required empty public constructor
    }


    /**
     * fragment lifecycle method for inflating the layout for this fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_token, container, false);
    }


    /**
     * fragment lifecycle method called after fragment is attached to a activity all view type casting and data binding will do here
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*all views are type casted here*/
        View view = getView();

        if(view != null){

            pieChart = view.findViewById(R.id.pie_chart);
        }

        /*add PieEntry to the list of PieEntry*/

        ArrayList<PieEntry> entries = new ArrayList<>();

        /*add the value with PieEntry object*/
        entries.add(new PieEntry(50 ));
        entries.add(new PieEntry(30));
        entries.add(new PieEntry(20));
        entries.add(new PieEntry(10));
        entries.add(new PieEntry(10));

        /*make pieDataSet with options and vlaues */
        PieDataSet pieDataSet = new PieDataSet(entries,"");
        pieDataSet.setValueTextSize(12);
        pieDataSet.setValueTextColor(Color.WHITE);
        pieDataSet.setValueLineColor(Color.WHITE);
        pieDataSet.setSliceSpace(2f);
        pieDataSet.setColors(ColorTemplate.createColors(getResources(), new int[]{R.color.one, R.color.two, R.color.three, R.color.four, R.color.five}));
        PieData pieData = new PieData(pieDataSet);



        /*make custom Value formatter for pieData*/
        pieData.setValueFormatter(new MyValueFormatter());
        /*set options to pieChart*/
        pieChart.setDrawEntryLabels(false);
        pieChart.setData(pieData);
        pieChart.getDescription().setEnabled(false);
        pieChart.setDrawHoleEnabled(false);
        pieChart.getLegend().setEnabled(false);



    }
}
