package ico.iss.com.ico.AdapterPackage;

/*all used classes are imported here*/

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ico.iss.com.ico.R;

public class TagRecycler extends RecyclerView.Adapter<TagRecycler.TagHolder> {


    /*global filed instances are here*/
    Random random = new Random();

    List<String> tags = new ArrayList<>();

    Context context;

    /*costractor for getting context from the activity*/
    public TagRecycler(Context context) {

        this.context = context;

        /*adding dubby tags items to the list*/
        tags.add("Investment");
        tags.add("Banking");
        tags.add("App");
        tags.add("Crypto Currency");

    }

    /**
     * inflate the specified layout for a row
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public TagHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tag_custom_layout, parent, false);

        return new TagHolder(view);
    }

    /**
     * binding data to every row
     * we have a random number generator that is getting int value from 0 to 4 for every different value we are setting different color to each row
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull TagHolder holder, int position) {

        holder.tagText.setText(tags.get(position));

        switch (randomColor()) {
            case 0:
                holder.tagBackground.setCardBackgroundColor(context.getResources().getColor(R.color.one));
                break;
            case 1:
                holder.tagBackground.setCardBackgroundColor(context.getResources().getColor(R.color.two));
                break;
            case 2:
                holder.tagBackground.setCardBackgroundColor(context.getResources().getColor(R.color.three));
                break;

            case 3:
                holder.tagBackground.setCardBackgroundColor(context.getResources().getColor(R.color.four));
                break;

            case 4:
                holder.tagBackground.setCardBackgroundColor(context.getResources().getColor(R.color.five));
                break;


        }

    }


    /**
     * retrun the size of the list
     * @return
     */

    @Override
    public int getItemCount() {

        return tags.size();
    }

    /**
     * return the random number value
     * @return
     */
    private int randomColor() {

        return random.nextInt(5);

    }

    /**
     * this class is the custom view holder for every row
     */
    class TagHolder extends RecyclerView.ViewHolder {

        CardView tagBackground;
        TextView tagText;

        public TagHolder(View itemView) {

            super(itemView);

            tagBackground = itemView.findViewById(R.id.container);

            tagText = itemView.findViewById(R.id.tag_text);
        }


    }
}
