package ico.iss.com.ico.AdapterPackage;
/*all used classes are imported here*/
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ico.iss.com.ico.R;

public class ReferAdapter extends RecyclerView.Adapter<ReferAdapter.ReferHolder> {

    /*all global filed instances are here*/

    private static final int FOOTER_TYPE = 1;
    private static final int REGULAR_TYPE = 2;
    private static final int HEADER_TYPE = 3;


    /**
     * inflate the specified layout for a row, we have 3 different types of row one is header, footer and regular
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public ReferHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == REGULAR_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.refer_row, parent, false);
            return new ReferHolder(view);
        } else if (viewType == FOOTER_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.refer_footer_row, parent, false);
            return new ReferHolder(view);
        } else if (viewType == HEADER_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.refer_header_row, parent, false);
            return new ReferHolder(view);
        }

        return null;
    }

    /**
     * this function gives the ViewHolder and there all the data binding with the views happens
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull ReferHolder holder, int position) {


    }


    /**
     * return the current row type from the position
     * in our case the first row is the header
     * last row is the footer
     * and rest of the rows are regular
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {

        if (position == 0) {

            return HEADER_TYPE;

        } else if (position == 4) {

            return FOOTER_TYPE;

        } else {

            return REGULAR_TYPE;
        }
    }

    /**
     * return the size of the list
     * @return
     */

    @Override
    public int getItemCount() {
        return 5;
    }


    /**
     * class for making a custom viewHolder a row
     */
    class ReferHolder extends RecyclerView.ViewHolder {

        public ReferHolder(View itemView) {
            super(itemView);
        }
    }
}
