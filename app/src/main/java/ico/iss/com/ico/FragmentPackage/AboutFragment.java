package ico.iss.com.ico.FragmentPackage;

/*all used classes are imported here*/

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ico.iss.com.ico.AdapterPackage.TagRecycler;
import ico.iss.com.ico.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class AboutFragment extends Fragment {


    /*all global field instances are here*/

    RecyclerView tagRecycler;

    public AboutFragment() {
        // Required empty public constructor
    }

    /**
     * fragment lifecycle method for inflating the layout for this fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about, container, false);
    }


    /*type casting all the view from the layout of this fragment*/
    private void initView() {
        View view = getView();
        if (view != null) {
            tagRecycler = view.findViewById(R.id.tag_recycler);
        }

    }

    /**
     * fragment lifecycle method called after fragment is attached to a activity all view type casting and data binding will do here
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*view type casting*/
        initView();
        RecyclerViewBuilder();
    }

    /*recycler view builder for the tag list*/
    private void RecyclerViewBuilder() {

        /*set recycler view size fixed*/
        tagRecycler.setHasFixedSize(true);
        /*make grid layout manager with context and span count of 3*/
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        /*set layout manager to reycler view*/
        tagRecycler.setLayoutManager(gridLayoutManager);
        /*make the adapter */
        TagRecycler tagAdapter = new TagRecycler(getContext());
        /*set the adapter to the recycler view*/
        tagRecycler.setAdapter(tagAdapter);

    }
}
