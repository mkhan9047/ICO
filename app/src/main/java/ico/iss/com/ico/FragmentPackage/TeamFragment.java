package ico.iss.com.ico.FragmentPackage;

/*all used classes are imported here*/

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ico.iss.com.ico.AdapterPackage.TeamRecyclerAdapter;
import ico.iss.com.ico.R;


public class TeamFragment extends Fragment {

    /*all global field instances are here*/
    RecyclerView teamRecycler;

    public TeamFragment() {
        // Required empty public constructor
    }


    /**
     * fragment lifecycle method for inflating the layout for this fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_team, container, false);
    }

    /**
     * fragment lifecycle method called after fragment is attached to a activity all view type casting and data binding will do here
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*type casting all the views*/
        View view = getView();

        if (view != null) {
            teamRecycler = view.findViewById(R.id.team_recycler);
        }

        /*set options to recycler view*/
        teamRecycler.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);

        teamRecycler.setLayoutManager(gridLayoutManager);


        TeamRecyclerAdapter adapter = new TeamRecyclerAdapter();



        /*set adapter to recycler view */
        teamRecycler.setAdapter(adapter);

    }
}
