package ico.iss.com.ico.ActivityPackage;

/*all used classes are imported here*/

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import ico.iss.com.ico.FragmentPackage.LogInFragment;
import ico.iss.com.ico.FragmentPackage.RegisterFragment;
import ico.iss.com.ico.R;

public class AuthActivity extends AppCompatActivity {

    /*Global field instances are here*/

    ImageButton btn_back;
    Fragment fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);


        /*views typecasting here*/
        initView();

        /*attach LogInFragment as default */
        fragmentTransaction(new LogInFragment());

    }


    /*views typecasting here*/
    private void initView(){

        btn_back = findViewById(R.id.btn_back);

    }


    /**
     *replace fragment by a new fragment called fragment transaction
     * @param fragment
     */
    public void fragmentTransaction(Fragment fragment) {

        /*if current fragment is LogInFragment then hide back button else show back button*/
        if (fragment instanceof LogInFragment) {
            btn_back.setVisibility(View.GONE);
        } else {
            btn_back.setVisibility(View.VISIBLE);
        }

        /*assign fragment to the global fragment variable */
        this.fragment = fragment;


        /*replace fragment with FragmentTransaction Object*/
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.auth_container, fragment);
        fragmentTransaction.commit();
    }

    /**
     *called when user press back button
     */
    @Override
    public void onBackPressed() {

        /*if current fragment is Register Fragment then come back to log in fragment*/
        if (fragment instanceof RegisterFragment) {

            fragmentTransaction(new LogInFragment());

        } else if (fragment instanceof LogInFragment) {

            /*if current fragment is log in fragment then exit the app */

            finish();
        }

    }
}
