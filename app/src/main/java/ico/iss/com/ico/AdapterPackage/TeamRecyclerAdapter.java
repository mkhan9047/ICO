package ico.iss.com.ico.AdapterPackage;

/*all imported classes are */

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ico.iss.com.ico.R;

public class TeamRecyclerAdapter extends RecyclerView.Adapter<TeamRecyclerAdapter.TeamHolder> {

    /**
     * inflate the specified layout for a row
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public TeamHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_recycler_row, parent, false);
        return new TeamHolder(view);
    }
    /**
     * binding data to every row
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull TeamHolder holder, int position) {

    }

    /**
     * retrun the size of the list
     * @return
     */
    @Override
    public int getItemCount() {
        return 15;
    }

    /**
     * this class is the custom view holder for every row
     */
    class TeamHolder extends RecyclerView.ViewHolder{

        public TeamHolder(View itemView) {
            super(itemView);
        }
    }
}
