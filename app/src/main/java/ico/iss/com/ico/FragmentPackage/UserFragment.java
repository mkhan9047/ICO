package ico.iss.com.ico.FragmentPackage;

/*all used classes are imported here*/

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ico.iss.com.ico.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class UserFragment extends Fragment {


    public UserFragment() {
        // Required empty public constructor
    }

    /**
     * fragment lifecycle method for inflating the layout for this fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user, container, false);
    }

}
