package ico.iss.com.ico.FragmentPackage;

/*all used classes are imported here*/

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import ico.iss.com.ico.ActivityPackage.AuthActivity;
import ico.iss.com.ico.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class RegisterFragment extends Fragment {

    /*all global field instances are here*/
    TextView btn_login;

    public RegisterFragment() {
        // Required empty public constructor
    }


    /**
     * fragment lifecycle method for inflating the layout for this fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }


    /**
     * fragment lifecycle method called after fragment is attached to a activity all view type casting and data binding will do here
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        /*type casting all the views*/
        View view = getView();

        if (view != null) {

            btn_login = view.findViewById(R.id.btn_login);

        }

        /*click listener for log in button */
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*replace current fragment by logInFragment in AuthActivity*/
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    ((AuthActivity) Objects.requireNonNull(getActivity())).fragmentTransaction(new LogInFragment());
                }
            }
        });
    }
}
