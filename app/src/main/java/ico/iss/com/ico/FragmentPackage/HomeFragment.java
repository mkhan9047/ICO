package ico.iss.com.ico.FragmentPackage;

/*all used classes are imported here*/

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ico.iss.com.ico.AdapterPackage.DashboardViewPagerAdapter;
import ico.iss.com.ico.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class HomeFragment extends Fragment {

    /*all global field instances are here*/
    ViewPager viewPager;
    TabLayout tabLayout;
    DashboardViewPagerAdapter adapter;
    RelativeLayout relativeLayout;


    public HomeFragment() {
        // Required empty public constructor
    }


    /**
     * fragment lifecycle method for inflating the layout for this fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.coordinator_layout_home, container, false);
    }

    /**
     * fragment lifecycle method called after fragment is attached to a activity all view type casting and data binding will do here
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);


        /*all views type casting*/
        initView();

        /*set the options for view pager*/
        initViewPager();



        /*set view pager to tab layout*/
        tabLayout.setupWithViewPager(viewPager);


        /*for every tab fo the tablayout set the custom view*/
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            relativeLayout = (RelativeLayout) LayoutInflater.from(getActivity()).inflate(R.layout.test_2, tabLayout, false);
            TextView textView = relativeLayout.findViewById(R.id.tab_title);
            textView.setText(adapter.getPageTitle(i));
            tab.setCustomView(relativeLayout);
            tab.select();

         /*   ImageView imageView = relativeLayout.findViewById(R.id.handle);
            if (i != 0) {
                imageView.setVisibility(View.INVISIBLE);
                textView.setTextColor(getResources().getColor(R.color.text_tab_default));
            } else {
                textView.setTextColor(getResources().getColor(R.color.text_tab_hover));
                imageView.setVisibility(View.VISIBLE);
            }*/
        }


        /*set view pager 0 as default */
        indicatorChanger(0);
        viewPager.setCurrentItem(0);

        /*tab layout on tab selected listener*/
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                /*call the tab indicator changer function by position of the tab*/
                indicatorChanger(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    /*change the indicator of the tab to the selected position*/
    private void indicatorChanger(int position) {


        /*get every tab from the tab layout */
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            /*get the image and title from the custom layout*/
            ImageView imageView = tabLayout.getTabAt(i).getCustomView().findViewById(R.id.handle);
            TextView title = tabLayout.getTabAt(i).getCustomView().findViewById(R.id.tab_title);


            /*if not current position*/
            if (position != i) {
                /*set text color to default*/
                /*set indicator image invisible*/
                title.setTextColor(getResources().getColor(R.color.text_tab_default));
                imageView.setVisibility(View.INVISIBLE);

            } else {
                /*set text color to hover*/
                /*set indicator image visible*/
                title.setTextColor(getResources().getColor(R.color.text_tab_hover));
                imageView.setVisibility(View.VISIBLE);
            }
        }


    }

    /*set options to view pager*/

    private void initViewPager() {

        /*make the view pager adapter*/
        adapter = new DashboardViewPagerAdapter(getChildFragmentManager());

        /*add fragments to view pager*/
        adapter.addFragment(new AboutFragment(), "About");
        adapter.addFragment(new TokenFragment(), "Token");
        adapter.addFragment(new RoadMapFragment(), "Roadmap");
        adapter.addFragment(new TeamFragment(), "Team");
        //adapter.addFragment(new WalletFragment(), "Rating");

        /*add adapter to view pager*/
        viewPager.setAdapter(adapter);

    }

    /*type casting all the views*/

    private void initView() {
        View view = getView();
        if (view != null) {
            viewPager = view.findViewById(R.id.dashboard_viewpager);
            tabLayout = view.findViewById(R.id.tab_layout);
        }


    }
}
