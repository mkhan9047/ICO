package ico.iss.com.ico.HelperPackage;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.widget.Toast;

public class Util {
    /**
     * convert density pixel to pixel
     * @param context
     * @param dp
     * @return
     */
    public static float DpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    /**
     * return the flie name from the uri
     * @param uri
     * @param context
     * @return
     */
    public static String getFileNameFromUri(Uri uri, Context context) {
        /*Take a placeholder String and assign null*/
        String result = null;
        /*if the Scheme of the uri is equals to content then make a query with the URI by the help of ContentResolver and assign a cursor with the result*/
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                /*if cursor is not null and move the cursor the first row and if it successs */
                if (cursor != null && cursor.moveToFirst()) {
                    /*assign the result by taking the file name column from the cursor by a Constant field of OpenableColumns */
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                /*if cursor is not null then close the cursor*/
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        /*if result is null*/
        if (result == null) {
            /*keep uri path in the result variable*/
            result = uri.getPath();
            /*assign cut variable with the last index of the uri path */
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                /*if cut is not -1 then assign result variable with the substring of cut and add the 1 more character with that */
                result = result.substring(cut + 1);
            }
        }
        /*return the result*/
        return result;

    }


    /*copy the text to clipboard*/
    public static void copyToClipboard(String copyText, Activity context) {
        /*uses clipboardManager for storing the text to the clipboard*/
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager)
                context.getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clip = android.content.ClipData
                .newPlainText("Your url", copyText);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }

        Toast toast = Toast.makeText(context,
                "Your url is copied", Toast.LENGTH_SHORT);
        toast.show();

    }
}
