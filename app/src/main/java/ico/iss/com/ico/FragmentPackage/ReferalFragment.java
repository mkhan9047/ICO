package ico.iss.com.ico.FragmentPackage;

/*all used classes are imported here*/
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import ico.iss.com.ico.AdapterPackage.ReferAdapter;
import ico.iss.com.ico.HelperPackage.Util;
import ico.iss.com.ico.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class ReferalFragment extends Fragment {


    /*all global field instances are here*/
    ImageButton copyButton;
    TextView url;
    RecyclerView referRecycler;

    public ReferalFragment() {
        // Required empty public constructor
    }


    /**
     * fragment lifecycle method for inflating the layout for this fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_referal, container, false);
    }

    /**
     * fragment lifecycle method called after fragment is attached to a activity all view type casting and data binding will do here
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*type casting all the views*/
        initView();

        /*click listener for copy button */
        copyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*copy the text from url edittext input to clipboard*/

                if (getActivity() != null)
                    Util.copyToClipboard(url.getText().toString(), getActivity());

            }
        });

        /*set options to recylerview */
        referRecycler.setHasFixedSize(true);
        referRecycler.setNestedScrollingEnabled(false);
        referRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        ReferAdapter referAdapter = new ReferAdapter();
        referRecycler.setAdapter(referAdapter);
    }


    /*type casting all the views*/
    private void initView() {

        View view = getView();

        if (view != null) {

            url = view.findViewById(R.id.url);
            copyButton = view.findViewById(R.id.copy_button);
            referRecycler = view.findViewById(R.id.refer_recycler);
        }

    }
}
