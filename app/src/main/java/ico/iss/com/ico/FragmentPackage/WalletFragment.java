package ico.iss.com.ico.FragmentPackage;

/*all used classes are imported here*/

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import ico.iss.com.ico.ActivityPackage.DashboardActivity;
import ico.iss.com.ico.AdapterPackage.WalletRecyclerAdapter;
import ico.iss.com.ico.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class WalletFragment extends Fragment {

    /*all global field instances are here*/
    RecyclerView wallet_recycler;
    Button deposit, withdraw;

    public WalletFragment() {
        // Required empty public constructor
    }


    /**
     * fragment lifecycle method for inflating the layout for this fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wallet, container, false);
    }

    /**
     * fragment lifecycle method called after fragment is attached to a activity all view type casting and data binding will do here
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);


        /*all views are type casted here*/
        View view = getView();

        if (view != null) {

            deposit = view.findViewById(R.id.deposit_btn);
            withdraw = view.findViewById(R.id.withdraw_btn);
            wallet_recycler = view.findViewById(R.id.wallet_recycler);

        }

        /*deposit button click listener */
        deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                    /*replace current fragment with Deposit Fragment*/
                    ((DashboardActivity) Objects.requireNonNull(getActivity())).FragmentTransaction(new DepositFragment());
                }
            }
        });

        /*withdraw button click listener*/

        withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    /*replace current fragment with withdraw fragment*/
                    ((DashboardActivity) Objects.requireNonNull(getActivity())).FragmentTransaction(new WithdrawFragment());
                }
            }
        });

        //  wallet_recycler.setHasFixedSize(true);

        /*set options to recycler view */
        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        gridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);

        wallet_recycler.setLayoutManager(gridLayoutManager);

        wallet_recycler.setNestedScrollingEnabled(false);


        /*make adapter for recycler view */
        WalletRecyclerAdapter walletRecyclerAdapter = new WalletRecyclerAdapter(getActivity());

        /*set the adapter to recycler view */
        wallet_recycler.setAdapter(walletRecyclerAdapter);
    }


}
