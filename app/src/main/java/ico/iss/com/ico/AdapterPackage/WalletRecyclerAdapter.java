package ico.iss.com.ico.AdapterPackage;
/*all used classes are imported here*/

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ico.iss.com.ico.ActivityPackage.DashboardActivity;
import ico.iss.com.ico.FragmentPackage.AddWalletFragment;
import ico.iss.com.ico.ModelPackage.Wallet;
import ico.iss.com.ico.R;

public class WalletRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /*all global field instances are here*/
    private List<Wallet> WalletList;
    Random random = new Random();
    private int TYPE_REGULAR = 0;
    private int TYPE_ADD_WALLET = 1;
    Activity context;
    private int count = 1;

    /*constructor for getting context from the activity*/
    public WalletRecyclerAdapter(Activity context) {

        this.context = context;

        /*making the WalletList */
        WalletList = new ArrayList<>();

        /*adding wallet to wallet list as dummy*/
        WalletList.add(new Wallet(345.4515, "BTC", "BTC Wallet Balance"));
        WalletList.add(new Wallet(445.455, "TCN", "TCN Wallet Balance"));
        WalletList.add(new Wallet(645.4515, "USD", "USD Wallet Balance"));

        /*ignore this one for the last row that is our Add Wallet row*/
        WalletList.add(new Wallet(00.00, "", ""));

    }

    /**
     * inflate the specified layout for a row, we have 2 different types of row one is REGULAR and AddWallet
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == TYPE_REGULAR) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wallet_recyler_row, parent, false);
            return new WalletHolder(view);
        } else if (viewType == TYPE_ADD_WALLET) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_wallet_reycler_row, parent, false);
            return new AddWalletHolder(view);
        }

        return null;

    }

    /**
     * all data binding with the every row
     * if current row holder is a WalletHolder then uses the WalletHolder and bind data to WalletHolder views
     * if current row holder is a AddWalletHolder then used the AddWalletHolder and bind data to AddWalletHolder
     * @param holder
     * @param position
     */
    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        /*check the current holder if it is Wallet holder*/
        if (holder instanceof WalletHolder) {


            /*cast he holder to the WalletHolder*/
            WalletHolder walletHolder = (WalletHolder) holder;

            /*bind data to wallet holder*/
            walletHolder.walletName.setText(WalletList.get(position).getWalletName());
            walletHolder.walletType.setText(WalletList.get(position).getWalletType());
            walletHolder.walletBalance.setText(String.format("%.4f", WalletList.get(position).getWalletBalance()));

            /*increase the value of holder*/
            count++;

            /*check the count */
            switch (count) {
                /*in case of 1 value change the background of the holder */
                case 1:

                    walletHolder.wallet_background.setBackgroundResource(R.drawable.wallet_type_graident_bg_one);
                    break;
                /*in case of 2 value change the background of the holder */
                case 2:
                    walletHolder.wallet_background.setBackgroundResource(R.drawable.wallet_type_gradient_bg_two);
                    break;
                /*in case of 3 value change the background of the holder */
                case 3:
                    count = 0;


                    walletHolder.wallet_background.setBackgroundResource(R.drawable.wallet_type_gradient_bg_three);
                    break;


            }

            /*if current holder is a AddWalletHolder then uses the AddWalletHolder class to bind data data*/
        } else if (holder instanceof AddWalletHolder) {

            /*cast holder tot he AddWalletHolder object*/
            AddWalletHolder addWalletHolder = (AddWalletHolder) holder;

            /*make AddHolder text underline*/
            addWalletHolder.addWallet.setPaintFlags(addWalletHolder.addWallet.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        }

    }

    /**
     * get the current view type by position
     * the last position row is a AddWalletType and rests are Regular type
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {

        if (position == WalletList.size() - 1) {
            return TYPE_ADD_WALLET;
        } else {
            return TYPE_REGULAR;
        }

    }

    /**
     * return the size of the WalletList
     * @return
     */
    @Override
    public int getItemCount() {

        return WalletList.size();

    }


    /*class for making the custom WalletHolder by extending view holder*/
    class WalletHolder extends RecyclerView.ViewHolder {
        /*all views instances*/
        TextView walletBalance, walletType, walletName;
        LinearLayout wallet_background;
        CardView wallet_card;

        public WalletHolder(View itemView) {
            super(itemView);
            /*type casting all the views*/
            walletBalance = itemView.findViewById(R.id.wallet_balance);
            walletName = itemView.findViewById(R.id.wallet_name);
            walletType = itemView.findViewById(R.id.wallet_type);
            wallet_card = itemView.findViewById(R.id.wallet_card);
            wallet_background = itemView.findViewById(R.id.wallet_background);
        }
    }

    /*class for making the custom WalletHolder by extending view holder*/
    class AddWalletHolder extends RecyclerView.ViewHolder {

        /*all views instances*/
        TextView addWallet;
        CardView walletCard;

        public AddWalletHolder(View itemView) {

            super(itemView);

            /*type casting all the views*/
            walletCard = itemView.findViewById(R.id.add_new_wallet_card);
            addWallet = itemView.findViewById(R.id.add_new_wallet);

            /*click listener for the AddWalletRow*/

            walletCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*replace the current set fragment by AddWalletFragment in DashboradActivity*/
                    ((DashboardActivity) context).FragmentTransaction(new AddWalletFragment());
                }
            });
        }
    }
}
