package ico.iss.com.ico.FragmentPackage;

/*all used classes are imported here*/

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import ico.iss.com.ico.ActivityPackage.AuthActivity;
import ico.iss.com.ico.ActivityPackage.DashboardActivity;
import ico.iss.com.ico.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class LogInFragment extends Fragment {

    /*all global field instances are here*/
    Button logIn;
    TextView btnSignUp;

    public LogInFragment() {
        // Required empty public constructor
    }


    /**
     * fragment lifecycle method for inflating the layout for this fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_log_in, container, false);
    }


    /**
     * fragment lifecycle method called after fragment is attached to a activity all view type casting and data binding will do here
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        /*type casting all the views*/
        View view = getView();

        if (view != null) {

            logIn = view.findViewById(R.id.btn_login);
            btnSignUp = view.findViewById(R.id.btn_signup);
        }

        /*Sign up button click listener*/

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*change the current fragment by RegisterFragment in AuthActivity*/
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    ((AuthActivity) Objects.requireNonNull(getActivity())).fragmentTransaction(new RegisterFragment());
                }
            }
        });


        /*click listener for log in button */
        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*go to the Dashboard from login activity*/
                Intent intent = new Intent(getActivity(), DashboardActivity.class);
                if (getActivity() != null)
                    getActivity().startActivity(intent);
            }
        });
    }
}
