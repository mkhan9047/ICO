package ico.iss.com.ico.ModelPackage;

public class Wallet {

    /*all global field instances are here*/
    private double walletBalance;
    private String walletType;
    private String walletName;

    /**
     * constructor  for building Wallet object
     * @param walletBalance
     * @param walletType
     * @param walletName
     */
    public Wallet(double walletBalance, String walletType, String walletName) {
        this.walletBalance = walletBalance;
        this.walletType = walletType;
        this.walletName = walletName;
    }


    /*getter methods for wallet*/
    public double getWalletBalance() {
        return walletBalance;
    }

    public String getWalletType() {
        return walletType;
    }

    public String getWalletName() {
        return walletName;
    }

}
